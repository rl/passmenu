passmenu
========

This is my fork of [zx2c4's passmenu][].

passmenu is a [dmenu][]-based interface to [pass][], the standard Unix password
manager. Its design allows you to quickly copy a password to the clipboard
without having to open up a terminal window if you don't already have one open. 

**The only difference in this fork is that passwords stored in `pass tfa/`-folder
will be copied  to clipboard using the `pass otp` extension instead of plain
`pass`. Additionally, the font is set to mononoki.**

# How to use

## Install prerequisite packages

`gpg` - for generating and managing PGP keys.

`pass` - the password store.

`pass-extension-otp` - extension for otp.

`dmenu` - for spawning passmenu.

## Optional

`nerd-fonts-mononoki` - the font this script uses

`zbarimg` - to copy otp urls from qr codes.

## Set up pass

Normal passwords can go in any folder or directly in pass root folder. 
When you store a otp url, put these in a pass folder named `tfa`, for example, 
if you downloaded a qr code image with the name canvas.png:

`zbarimg canvas.png`

Copy url from `otpauth://...` (i.e. don't include `QR-Code:`)

Then run:

`pass otp add tfa/name-of-website-here`

And paste the otpauth URI you copied previously when prompted.

## Put `passmenu` script in a folder that is in your users path and set a
shortcut for it to launch.

Now when copying from any other folder than the tfa folder pass will use the
normal `pass -c somde-folder-name/name-of-website` syntax and for the passwords
in tfa folder it will use `pass otp -c tfa/name-of-website`.

---

[zx2c4's passmenu]: https://git.zx2c4.com/password-store/tree/contrib/dmenu
[dmenu]: http://tools.suckless.org/dmenu/
[pass]: http://www.zx2c4.com/projects/password-store/
[pass-otp]: https://github.com/tadfisher/pass-otp
